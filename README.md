# RSNA

Pneumonia detection, classification and bounding box localisation, using chest radiographs.

For more information, please refer to https://www.kaggle.com/c/rsna-pneumonia-detection-challenge/