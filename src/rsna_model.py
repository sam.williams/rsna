import keras
from rsna_loss_function import RSNA_Loss

inp = keras.layers.Input(shape=(1024,1024,1))

x = keras.layers.Conv2D(16, (7,7), strides=(5,5))(inp)
x = keras.layers.MaxPool2D((2,2), strides=(2,2))(x)
x = keras.layers.Conv2D(32, (9,9))(x)
x = keras.layers.MaxPool2D((2,2), strides=(2,2))(x)
x = keras.layers.Conv2D(32, (7,7))(x)
x = keras.layers.MaxPool2D((2,2), strides=(2,2))(x)
output = keras.layers.Conv2D(5, (2,2), strides=(2,2))(x)

rsna_model = keras.Model(inp,output)
rsna_model.compile(optimizer="adam", loss=RSNA_Loss(name="rsna_loss"))