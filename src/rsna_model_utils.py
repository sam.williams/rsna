import os
import glob
import itertools
import math
import numpy as np
import pandas as pd
from pydicom.filereader import dcmread
import matplotlib.pyplot as plt
import cv2



def pixel_and_meta_data(file):
    file = dcmread(file)
    pixel_array = np.expand_dims(file.pixel_array,-1)
    return file, pixel_array/255



def plot_image(file, df_train_labels, model=None):
    
    val = 1024/10
    file, pixel_array = pixel_and_meta_data(file)
    df = df_train_labels.loc[file.PatientID]

    if model is not None:
        test_data_array = []
        test_sample = np.expand_dims(pixel_array,0)
        result = model.predict(test_sample)
        mx = np.max(result[0,:,:,0])
  
        for i in range(10):
            for j in range(10):
                res = result[0,i,j,:]
                if res[0] > 0.1:
                    x1 = int(val*(j + res[1] -0.5*res[3]))
                    y1 = int(val*(i + res[2] -0.5*res[4]))
                    x2 = int(val*(j + res[1] +0.5*res[3]))
                    y2 = int(val*(i + res[2] +0.5*res[4]))
                    pixel_array = cv2.rectangle(pixel_array, (x1,y1), (x2,y2), (0,0,0), 3)
                    test_data_array.append([res[0],x1,y1,int(x2-x1), int(y2-y1)])
        
    
    data_array = []
    if type(df) == pd.core.frame.DataFrame:
        for im in range(df.shape[0]):
            x1,y1,width,height,Target = df.iloc[im].tolist()
            x2,y2=int(x1+width), int(y1+height)
            data_array.append([1, x1, y1, width, height])
            pixel_array = cv2.rectangle(pixel_array, (int(x1),int(y1)), (x2,y2), (1,1,1), 3)
    else:
        if df.Target == 1:
            data_array.append([1, df.x, df.y, df.width, df.height])
            pixel_array = cv2.rectangle(pixel_array, (int(df.x),int(df.y)), (int(df.x+df.width),int(df.y+df.height)), (1,1,1), 3)

    if model is not None:
        for data in data_array: 
            print(f"True Labels: {data}")
       
        print("")
        for data in test_data_array:
            print(f"Predicted Labels: {data}")
        print("")
    plt.figure(figsize=(12,8))
    plt.imshow(np.squeeze(pixel_array), cmap="bone")
    plt.axis("off")
    plt.show()



def create_label(ID, df_train_labels):
    labels = np.zeros(shape=(10,10,5))
    val = 1024/10
    df = df_train_labels.loc[ID]
    
    data_array = []
    if type(df) == pd.core.frame.DataFrame:
        for im in range(df.shape[0]):
            data_array.append(df.iloc[im].tolist())
    else:
        if df.Target == 1:
            data_array.append(df.tolist())
    
    for data in data_array:
        x,y,width,height,Target = data
        x += 0.5*width
        y += 0.5*height
        x_grid = int(x/val)
        y_grid = int(y/val)
        labels[x_grid,y_grid,:] = [1, (x/val)-x_grid, (y/val)-y_grid, width/val, height/val]
    return labels


def chunk(array, size=64):
    return list(itertools.islice(array, size))


def rsna_generator(medical_files_train, df_train_labels):
    while True:
        inputs = []
        outputs = []
        for im in chunk(medical_files_train):
            file, input_pixels = pixel_and_meta_data(im)
            output = create_label(file.PatientID, df_train_labels)
            inputs.append(input_pixels)
            outputs.append(output)
        yield ({'input_1': np.stack(inputs)}, {'conv2d_4': np.stack(outputs)})
