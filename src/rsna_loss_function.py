from keras.losses import Loss
from keras import backend as K





class RSNA_Loss(Loss):

    def call(self, y_true, y_pred):
        """  
        Tensor shape of output: (batch_size, 10, 10, 5)
        
        (i, j, k, :) : [P, bx, by, bw, bh]
        
        P: Probability the centre of a bounding box, for a predicted lung opacity, is located in this grid position (j,k), for batch i.
        bx: Bounding box centre x.
        by: Bounding box centre y.
        bw: Bounding box width.
        bh: Bounding box height.

        """

        lambda_1 = 5
        lambda_noobj = 10

        x_1_diff = y_true[:,:,:,1] - y_pred[:,:,:,1]
        y_1_diff = y_true[:,:,:,2] - y_pred[:,:,:,2]

        w_1_diff = K.sqrt(y_true[:,:,:,3]) - K.sqrt(y_pred[:,:,:,3])
        h_1_diff = K.sqrt(y_true[:,:,:,4]) - K.sqrt(y_pred[:,:,:,4])


        loss_1 = lambda_1*K.sum(y_true[:,:,:,0] * (K.square(x_1_diff) + K.square(y_1_diff)))

        loss_2 = lambda_1*K.sum(y_true[:,:,:,0] * (K.square(w_1_diff) + K.square(h_1_diff)))

        loss_3 = lambda_noobj*K.sum(y_true[:,:,:,0] * (K.square(y_true[:,:,:,0] - y_pred[:,:,:,0])))

        loss_4 = lambda_noobj*K.sum((y_true[:,:,:,0]*(-1) + 1) * (K.square(y_true[:,:,:,0] - y_pred[:,:,:,0])))

        return loss_1 + loss_2 + loss_3 + loss_4